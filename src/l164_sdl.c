#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
#include "lstate.h"

#define USE_SDL 1

#if USE_SDL
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

////////////////////////////////// SDL stuff //////////////////////////////////
//
int sdl_initialized = 0;
void init_SDL() {
  if (!sdl_initialized) {
    SDL_Init(SDL_INIT_EVERYTHING);
    sdl_initialized = 1;
  }
}

int lua_IMG_Load(lua_State *L) {
  const char *file = luaL_checkstring(L, 1);
  SDL_Surface *ret = IMG_Load(file);

  lua_settop(L, 0);
  lua_pushlightuserdata(L, ret);

  return 1;
}

int lua_SDL_FreeSurface(lua_State *L) {
  SDL_Surface *surface = lua_touserdata(L, 1);
  SDL_FreeSurface(surface);

  lua_settop(L, 0);
  return 0;
}

int lua_boot_SDL(lua_State *L) {
  init_SDL();
  SDL_Surface *ret = SDL_SetVideoMode(640, 480, 32, SDL_SWSURFACE);

  lua_settop(L, 0);
  lua_pushlightuserdata(L, ret);

  return 1;
}

int lua_SDL_Flip(lua_State *L) {
  SDL_Surface *screen = lua_touserdata(L, 1);
  int ret = SDL_Flip(screen);

  lua_settop(L, 0);
  lua_pushinteger(L, ret);

  return 1;
}

int lua_SDL_Delay(lua_State *L) {
  Uint32 ms = luaL_checkinteger(L, 1);
  SDL_Delay(ms);

  lua_settop(L, 0);
  return 0;
}
void SDL_Rect_lua_to_c(lua_State *L, int idx, SDL_Rect *out) {
  lua_pushstring(L, "x");
  lua_gettable(L, idx);
  out->x = lua_tointeger(L, -1);
  lua_pop(L, 1);
  lua_pushstring(L, "y");
  lua_gettable(L, idx);
  out->y = lua_tointeger(L, -1);
  lua_pop(L, 1);
  lua_pushstring(L, "w");
  lua_gettable(L, idx);
  out->w = lua_tointeger(L, -1);
  lua_pop(L, 1);
  lua_pushstring(L, "h");
  lua_gettable(L, idx);
  out->h = lua_tointeger(L, -1);
  lua_pop(L, 1);
}

void SDL_Rect_c_to_lua(lua_State *L, int idx, const SDL_Rect *in) {
  lua_pushstring(L, "x");
  lua_pushinteger(L, in->x);
  lua_settable(L, idx);
  lua_pushstring(L, "y");
  lua_pushinteger(L, in->y);
  lua_settable(L, idx);
  lua_pushstring(L, "w");
  lua_pushinteger(L, in->w);
  lua_settable(L, idx);
  lua_pushstring(L, "h");
  lua_pushinteger(L, in->h);
  lua_settable(L, idx);
}

int lua_SDL_BlitSurface(lua_State *L) {
  SDL_Surface *src = lua_touserdata(L, 1);
  int has_srcrect = !lua_isnil(L, 2);
  SDL_Rect srcrect;
  if (has_srcrect) {
    SDL_Rect_lua_to_c(L, 2, &srcrect);
  }
  SDL_Surface *dst = lua_touserdata(L, 3);
  int has_dstrect = !lua_isnil(L, 4);
  SDL_Rect dstrect;
  if (has_dstrect) {
    SDL_Rect_lua_to_c(L, 4, &dstrect);
  }
  int ret = SDL_BlitSurface(src, has_srcrect ? &srcrect : NULL,
                            dst, has_dstrect ? &dstrect : NULL);

  lua_settop(L, 0);
  lua_pushinteger(L, ret);
  if (has_dstrect) {
    lua_newtable(L);
    SDL_Rect_c_to_lua(L, lua_gettop(L), &dstrect);
  }

  return 1 + has_dstrect;
}

int lua_get_event(lua_State *L) {
  SDL_Event event;
  int ret = SDL_PollEvent(&event);

  lua_settop(L, 0);
  lua_pushboolean(L, ret);
  if (!ret) {
    return 1;
  }

  // There is an event
  lua_newtable(L);
  switch (event.type) {
    case SDL_QUIT:
      lua_pushstring(L, "type");
      lua_pushstring(L, "quit");
      lua_settable(L, -3);
      break;
    case SDL_KEYDOWN:
    case SDL_KEYUP:
      lua_pushstring(L, "type");
      if (event.type == SDL_KEYDOWN)
        lua_pushstring(L, "keydown");
      else
        lua_pushstring(L, "keyup");
      lua_settable(L, -3);

      lua_pushstring(L, "state");
      if (event.key.state == SDL_PRESSED)
        lua_pushstring(L, "pressed");
      else
        lua_pushstring(L, "released");
      lua_settable(L, -3);

      lua_pushstring(L, "scancode");
      lua_pushinteger(L, event.key.keysym.scancode);
      lua_settable(L, -3);
      lua_pushstring(L, "sym");
      lua_pushinteger(L, event.key.keysym.sym);
      lua_settable(L, -3);
      lua_pushstring(L, "mod");
      lua_pushinteger(L, event.key.keysym.mod);
      lua_settable(L, -3);
      lua_pushstring(L, "unicode");
      lua_pushinteger(L, event.key.keysym.unicode);
      lua_settable(L, -3);

      break;
    default:
      lua_pushstring(L, "type");
      lua_pushinteger(L, event.type);
      lua_settable(L, -3);
      break;
  }

  return 2;
}

int lua_SDL_GetTicks(lua_State *L) {
  Uint32 ret = SDL_GetTicks();

  lua_settop(L, 0);
  lua_pushinteger(L, ret);

  return 1;
}

#define SDL_FUNCS \
  {"IMG_Load", lua_IMG_Load}, \
  {"SDL_FreeSurface", lua_SDL_FreeSurface}, \
  {"boot_SDL", lua_boot_SDL}, \
  {"SDL_Flip", lua_SDL_Flip}, \
  {"SDL_Delay", lua_SDL_Delay}, \
  {"SDL_BlitSurface", lua_SDL_BlitSurface}, \
  {"get_event", lua_get_event}, \
  {"SDL_GetTicks", lua_SDL_GetTicks},

#endif

#include "l164.c"
