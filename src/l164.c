#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <zmq.h>
#include <stdbool.h>

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
#include "lstate.h"

#define PATCH_TABLE_NAME "__patch_table"
#define PATCH_VERSIONS_TABLE_NAME "__patch_versions_table"

LClosure * lookup_closure(lua_State * L, const char * name) {
  lua_getglobal(L, PATCH_TABLE_NAME);
  lua_pushstring(L, name);
  lua_gettable(L, -2);
  LClosure * c = lua_tolclosure(L, -1);
  lua_pop(L, 2); /* Pop the closure and the table off. */
  return c;
}

void set_closure(lua_State *L, const char * name, LClosure * c) {
  lua_getglobal(L, PATCH_TABLE_NAME);
  lua_pushstring(L, name);
  lua_pushlclosure(L, c);
  lua_settable(L, -3);
  lua_pop(L, 1); /* Pop the table off. */
}

void save_closure(lua_State *L, const char * name, LClosure * c) {
  lua_getglobal(L, PATCH_VERSIONS_TABLE_NAME);
  lua_pushstring(L, name);
  lua_gettable(L, -2);
  if (lua_isnil(L, -1)) {
    lua_pop(L, 1); /* Remove the nil. */
    lua_pushstring(L, name);
    lua_newtable(L);
    assert(lua_istable(L, -1));
    lua_settable(L, -3);
    lua_pushstring(L, name);
    lua_gettable(L, -2);
  }
  assert(lua_istable(L, -1));

  lua_pushinteger(L, luaL_len(L, -1) + 1);

  lua_pushlclosure(L, c);
  lua_settable(L, -3);
  lua_pop(L, 2); /* Pop the two tables off. */
}

Instruction *patch_prefix_end(Instruction *newi, Instruction *old, Instruction *old_end) {
  int i;
  for (i = 0; old + i + 1 < old_end && newi[i] == old[i]; i++) {}
  return old + i;
}

void patch_prototype(Proto *old_proto, Proto *new_proto) {
  if (old_proto->numparams != new_proto->numparams ||
      old_proto->is_vararg != new_proto->is_vararg ||
      old_proto->sizeupvalues != new_proto->sizeupvalues) {
    printf("Patching of function prototype failed.\n");
    return;
  }
  old_proto->sizek = new_proto->sizek;
  old_proto->sizecode = new_proto->sizecode;
  old_proto->sizelineinfo = new_proto->sizelineinfo;
  old_proto->sizep = new_proto->sizep;
  old_proto->sizelocvars = new_proto->sizelocvars;
  old_proto->linedefined = new_proto->linedefined;
  old_proto->lastlinedefined = new_proto->lastlinedefined;
  old_proto->k = new_proto->k;
  old_proto->code = new_proto->code;
  old_proto->p = new_proto->p;
  old_proto->lineinfo = new_proto->lineinfo;
  old_proto->locvars = new_proto->locvars;
  old_proto->upvalues = new_proto->upvalues;
  old_proto->source = new_proto->source;
}

int lua_patch(lua_State * L) {
  const char * name = luaL_checkstring(L, 1);
  LClosure * new_closure = lua_tolclosure(L, 2);
  lua_pop(L, 2);
  if (!name || !new_closure) {
    lua_pushstring(L, "Missing argument to patch function.\n");
    return lua_error(L);
  }
  save_closure(L, name, new_closure);
  LClosure * old_closure = lookup_closure(L, name);
  if (old_closure) {
    patch_prototype(old_closure->p, new_closure->p);
    lua_pushlclosure(L, old_closure);
  } else {
    set_closure(L, name, new_closure);
    lua_pushlclosure(L, new_closure);
  }
  return 1;
}



int patch_mode = 0;

int lua_is_patching(lua_State *L) {
  lua_settop(L, 0);
  lua_pushboolean(L, patch_mode);
  return 1;
}

int lua_start(lua_State *L) {
  if (patch_mode) {
    /* Do nothing. */
    lua_pop(L, 1);
  } else {
    lua_call(L, 0, 0);
  }
  return 0;
}


static const luaL_Reg cs164_funcs[] = {
#ifdef SDL_FUNCS
  SDL_FUNCS
#endif
  {"patch", lua_patch},
  {"is_patching", lua_is_patching},
  {"start", lua_start},
  {NULL, NULL}
};

int luaopen_cs164(lua_State *L) {
  lua_newtable(L);
  lua_setglobal(L, PATCH_TABLE_NAME);
  lua_newtable(L);
  lua_setglobal(L, PATCH_VERSIONS_TABLE_NAME);
  luaL_newlib(L, cs164_funcs);
  return 1;
}

//////////////////////////////// End SDL stuff ////////////////////////////////

///////////////////////////// Debug protocol stuff /////////////////////////////


const char *print_traceback(lua_State *L) {
  lua_checkstack(L, 20);  // Herp?
  const char *msg = lua_tostring(L, -1);
  if (msg == NULL) {  /* is error object not a string? */
    if (luaL_callmeta(L, -1, "__tostring") &&  /* does it have a metamethod */
        lua_type(L, -1) == LUA_TSTRING) {  /* that produces a string? */
      /* that is the message */
      // Print the traceback
      const char *err_w_traceback = lua_tostring(L, -1);
      printf("%s\n", err_w_traceback);
    }
    else
      msg = lua_pushfstring(L, "(error object is a %s value)",
                               luaL_typename(L, -1));
  }
  luaL_traceback(L, L, msg, 0);  /* append a standard traceback */

  // Print the traceback
  const char *err_w_traceback = lua_tostring(L, -1);
  printf("%s\n", err_w_traceback);
  return err_w_traceback;
}

int pause_lua = 0;
int close_program = 0;


lua_State *main_state = NULL;
pthread_mutex_t main_state_mutex;
pthread_mutex_t main_lua_resume_mutex;
pthread_cond_t main_lua_resume;

const char * debug_address = "tcp://127.0.0.1:5555";


void *debug_thread_fn(void *useless) {
  (void)useless;

  void * context = zmq_ctx_new();
  void * responder = zmq_socket(context, ZMQ_REP);
  int rc = zmq_bind(responder, debug_address);
  if (rc != 0) {
    printf("Error listening on debug address '%s'.\n", debug_address);
    exit(0);
  }

  int packet_size = 8 * 1024 * 1024;
  char *c_buf = malloc(packet_size);
  if (c_buf == NULL) {
    printf("Could not allocate buffer for source code.\n");
    exit(0);
  }

  while (true) {
    memset(c_buf, 0, packet_size);
    zmq_recv(responder, c_buf, packet_size - 1, 0);
    patch_mode = 1;
    pause_lua = 1;
    pthread_mutex_lock(&main_state_mutex);
    lua_State * t = lua_newthread(main_state);
    int ret = luaL_loadstring(t, c_buf);
    if (ret != LUA_OK) {
      printf("Error loading patch:\n");
    } else {
      ret = lua_resume(t, NULL, 0);
    }
    if (ret == LUA_OK) {
      /* Ignore. */
      const char * msg = "{\"result\":\"Successfully patched.\"}";
      zmq_send(responder, msg, strlen(msg), 0);
    } else if (ret == LUA_YIELD) {
      printf("Top-level coroutine yielded!\n");
      const char * msg = "{\"error\":\"Top level coroutine yielded!\"}";
      zmq_send(responder, msg, strlen(msg), 0);
      exit(0);
    } else {
      /* Something broke. */
      const char * traceback = print_traceback(t);
      const char * format = "{\"error\": \"%s\"}";
      size_t s = strlen(format) + strlen(traceback) + 1;
      char * msg = malloc(s);
      if (!msg) {
        printf("Could not allocate error message.\n");
        exit(0);
      }
      snprintf(msg, s, traceback);
      zmq_send(responder, msg, s, 0);
      /* traceback freed by lua? */
      /* msg freed by zmq. */
    }
    pthread_mutex_unlock(&main_state_mutex);
    pthread_cond_signal(&main_lua_resume);
  }
  printf("Exiting debug thread.\n");

  return NULL;
}

/////////////////////////// End debug protocol stuff ///////////////////////////

void wait_hook_func(lua_State *L, lua_Debug *ar) {
  (void)ar;
  (void)L;

  if (pause_lua) {
    pause_lua = 0;
    pthread_mutex_unlock(&main_state_mutex);
    pthread_cond_wait(&main_lua_resume, &main_lua_resume_mutex);
    pthread_mutex_lock(&main_state_mutex);
  }
}

int main(int argc, char **argv) {
  if (argc < 2) {
    printf("Usage: %s file.lua\n", argv[0]);
    return 1;
  }

  pthread_mutex_init(&main_state_mutex, NULL);
  pthread_mutex_init(&main_lua_resume_mutex, NULL);
  pthread_cond_init(&main_lua_resume, NULL);

  pthread_t debug_thread;
  pthread_create(&debug_thread, NULL, debug_thread_fn, NULL);

  pthread_mutex_lock(&main_state_mutex);
  main_state = luaL_newstate();
  luaL_openlibs(main_state);
  luaL_requiref(main_state, "cs164", luaopen_cs164, 1);
  luaL_loadfile(main_state, argv[1]);

  lua_sethook(main_state, wait_hook_func, LUA_MASKCOUNT, 1);

  while (1) {
    int ret = lua_resume(main_state, NULL, 0);
    if (ret == LUA_OK) {
      break;
    } else if (ret == LUA_YIELD) {
      printf("Top-level coroutine yielded!\n");
      break;
    } else {
      // Something broke
      print_traceback(main_state);
      break;
    }
  }

  printf("Exiting main thread.\n");

  lua_close(main_state);

  return 0;    
}
