require 'cs164'
inspect = require 'inspect'

images = {}

if not cs164.is_patching() then
  screen = cs164.boot_SDL()
  sprite = {50, 50, 'dot'}
end

map = {
       27, 'sky', -1,
       21, 'sky', 2, 'block', 3, 'sky', 3, 'block', -1,
       22, 'sky', 1, 'block', 3, 'sky', 3, 'block', -1,
       21, 'sky', 2, 'block', 3, 'sky', 3, 'block', -1,
       22, 'sky', 1, 'block', 3, 'sky', 3, 'block', -1,
       22, 'sky', 1, 'block', 3, 'sky', 3, 'block', -1,
       22, 'sky', 1, 'block', 3, 'sky', 3, 'block', -1,
       22, 'sky', 1, 'block', 3, 'sky', 3, 'block', -1,
       22, 'sky', 1, 'block', 3, 'sky', 3, 'block', -1,
       22, 'sky', 1, 'block', 3, 'sky', 3, 'block', -1,
       21, 'sky', 2, 'block', 3, 'sky', 3, 'block', -1,
       22, 'sky', 1, 'block', 3, 'sky', 3, 'block', -1,
       22, 'sky', 1, 'block', 3, 'sky', 3, 'block', -1,
       22, 'sky', 1, 'block', 3, 'sky', 3, 'block', -1,
       22, 'sky', 1, 'block', 3, 'sky', 3, 'block', -1,
       22, 'sky', 1, 'block', 3, 'sky', 3, 'block', -1,
       22, 'sky', 1, 'block', 3, 'sky', 3, 'block', -1,
       27, 'sky', -1,
       27, 'sky', -1,
       27, 'block', -1,
       27, 'block', -1,
     }

grid = {}
dirty_grid = {}

height_px = 480
width_px = 640
px_per_grid_cell = 24
height = math.ceil(height_px / px_per_grid_cell)
width = math.ceil(width_px / px_per_grid_cell)

function print_grid(x, y)
  i = grid_idx(x, y)
  print('idx', i)
  print('grid[' .. i .. '] = ', grid[i])
end

function clear_dirty_grid()
  dirty_grid = {}
end

function mark_grid_dirty(x, y)
  dirty_grid[grid_idx(x, y)] = true
end

function mark_grid_dirty_around(x, y)
  for i = x - 1, x + 1 do
    for j = y - 1, y + 1 do
      dirty_grid[grid_idx(i, j)] = true
    end
  end
end

function grid_idx(x, y)
  return math.floor(x) + width * math.floor(y)
end

function grid_get(x, y)
  return grid[grid_idx(x, y)]
end

function grid_set(x, y, val)
  local idx = grid_idx(x, y)
  if grid[idx] ~= val then
    grid[idx] = val
    dirty_grid[idx] = true
  end
  return val
end

function map_to_grid(m)
  local x = 0
  local y = 0
  local i = 1
  while i < #m do
    if m[i] == -1 then
      y = y + 1
      x = 0
      i = i + 1
    else
      for j=1,m[i] do
        grid_set(x, y, m[i + 1])
        x = x + 1
      end
      i = i + 2
    end
  end
end
map_to_grid(map)


function load_image(name, filename)
  if images[name] then
    cs164.SDL_FreeSurface(images[name])
  end
  images[name] = cs164.IMG_Load(filename)
end

load_image('sky', 'sky.png')
load_image('block', 'block.png')
load_image('dot', 'Maxidleright.png')

downkeys = {}
jump_count = 0

function fall()
  local max = 18
  sprite_velocity[2] = sprite_velocity[2] + 3
  if sprite_velocity[2] > max then
    sprite_velocity[2] = max
  end
end
--fall = cs164.patch('fall', fall)

function jumpleft()
  --print 'jumpleft'
  if sprite_velocity[2] == 0 then
    load_image('dot', 'Maxwalkleft.png')
  end
  sprite_velocity[1] = -16
  --sprite[1] = sprite[1] - 16
  --sprite[2] = sprite[2] - 20
end

function jumpright()
  --print 'jumpright'
  if sprite_velocity[2] == 0 then
    load_image('dot', 'Maxwalkright.png')
  end
  sprite_velocity[1] = 16
  --sprite[1] = sprite[1] + 16
  --sprite[2] = sprite[2] - 20
end

function jumpup()
  if sprite_velocity[1] < 0 then
    load_image('dot', 'Maxjumpleft.png')
  else
    load_image('dot', 'Maxjumpright.png')
  end
  --sprite[2] = sprite[2] - 30
  --if sprite_velocity[2] > 0 and jump_count < 2 then
  if jump_count < 2 then
    jump_count = jump_count + 1
    --sprite_velocity[2] = -24 * math.log(jump_count + 1)
    --sprite_velocity[2] = math.floor(-24 * jump_count / math.sqrt(jump_count))
    --sprite_velocity[2] = math.floor(-23 * jump_count / math.sqrt(jump_count))
    sprite_velocity[2] = -23
    --math.floor(-23 * jump_count / math.sqrt(jump_count))
  end
end

function b(bool)
  if bool then
    return 1
  else
    return 0
  end
end

function sign(v)
  if v >= 0 then
    return 1
  else
    return -1
  end
end

function is_solid(x, y)
  --print('x, y =', x, y)
  local cell_type = grid_get(x / px_per_grid_cell, y/ px_per_grid_cell)
  --print('is_solid =', cell_type ~= 'sky')
  local res = not cell_type or cell_type ~= 'sky'
  --print('is_solid =', res)
  --print('cell_type =', cell_type)
  return res
end

sprite_size = 18

function sprite_collided()
  jump_count = 0
  load_image('dot', 'Maxidleright.png')
end

function round_to_zero(v)
  return sign(v) * math.floor(math.abs(v))
end

function move_sprite(s, v, depth)
  local v1, v2 = round_to_zero(v[1]), round_to_zero(v[2])
  if v1 == 0 and v2 == 0 then return end
  if not is_solid(s[1] + v1, s[2] + v2) and
     not is_solid(s[1] + v1 + sprite_size,
                  s[2] + v2 + sprite_size) then
    s[1] = s[1] + v1
    s[2] = s[2] + v2
  else
    move_sprite(s, {0.75 * v1, 0})
    move_sprite(s, {0, 0.75 * v2})
    sprite_collided()
  end
end

function draw_tile(t, x, y)
  cs164.SDL_BlitSurface(t, nil, screen, {x = x, y = y})
end

function draw_map(m)
  local x = 0
  local y = 0
  local i = 1
  while i < #m do
    if m[i] == -1 then
      y = y + 64
      x = 0
      i = i + 1
    else
      for j=1,m[i] do
        draw_tile(images[m[i + 1]], x, y)
        x = x + 64
      end
      i = i + 2
    end
  end
end

function draw_grid()
  for i=0, width do
    for j=0, height do
      draw_tile(images[grid[i + j * width]], i * px_per_grid_cell, j * px_per_grid_cell)
    end
  end
end

function draw_grid_dirty_only()
  local sky = images['sky']
  for k, _ in pairs(dirty_grid) do
    local x = ((k % width) * px_per_grid_cell)
    local y = (math.floor(k / width) * px_per_grid_cell)
    local image = images[grid[k]]
    --cs164.SDL_BlitSurface(t, {width = px_per_grid_cell, height = px_per_grid_cell, x = x, y = y}, screen, {x = x, y = y})
    --cs164.SDL_BlitSurface(t, nil, screen, {x = x, y = y, width = px_per_grid_cell, height = px_per_grid_cell})
    --print('drawing', grid[k])
    draw_tile(sky, x, y)
    draw_tile(image, x, y)
  end
  clear_dirty_grid()
end


function draw_sprite(s)
  mark_grid_dirty_around(sprite[1] / px_per_grid_cell, sprite[2] / px_per_grid_cell)
  draw_tile(images[sprite[3]],sprite[1],sprite[2])
end

sprite_velocity = {0, 0}

function main()
  local stay = true
  while stay do
    move_sprite(sprite, sprite_velocity)
    --draw_map(map)
    --print(inspect.inspect(grid))
    --print('starting to draw grid')
    draw_grid_dirty_only()
    --print('finished drawing grid')
    draw_sprite(sprite)
    cs164.SDL_Flip(screen)
    cs164.SDL_Delay(math.floor(1000/60))
    --cs164.SDL_Delay(math.floor(10000/60))
    for i=0,2 do
      local _, event = cs164.get_event()
      if event ~= nil then
        print(inspect.inspect(event))
        if event.state == 'pressed' then
          downkeys[event.scancode] = true
        end
        if event.state == 'released' then
          downkeys[event.scancode] = false
        end
        --if event.scancode == 113 then
          --jumpleft()
        --elseif event.scancode == 114 then
          --jumpright()
        if event.scancode == 111 and event.state == 'pressed' then
          jumpup()
        end
        if event.type == 'quit' then
          stay = false
        end
      end
    end

    sprite_velocity[1] = 0
    if downkeys[113] then
      jumpleft()
    end
    if downkeys[114] then
      jumpright()
    end
    --if downkeys[111] then
      --jumpup()
    --end
    if downkeys[65] then
      sprite[1] = 0
      sprite[2] = 0
    end
    fall()
  end
end


cs164.patch('main', main)

if not cs164.is_patching() then
  main()
else
  print 'patching'
end
