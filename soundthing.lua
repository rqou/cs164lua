-- TO RUN:
--  mkfifo soundfifo
--  ./src/l164 soundthing.lua
--  aplay -f S16_LE -c 1 -r 44100 soundfifo

local OUTF = io.open('soundfifo', 'wb')

function s(x) return string.pack("<h", math.floor(x)) end

local MASTER_F = 44100

function naive_sq(f, duration, min_a, max_a)
    -- hack
    coroutine.yield("hi")
    duration = duration or -1
    min_a = min_a or -32768
    max_a = max_a or 32767
    local ticks_between = MASTER_F / f
    local duration_ticks = MASTER_F // duration
    local x = 0
    local i = 0
    local out = false
    while i < duration_ticks or duration_ticks < 0 do
        x = x + 1
        if x >= ticks_between then
            x = 0
            out = not out
        end

        coroutine.yield(out and min_a or max_a)
    end
end

function cos_fn(f, duration, A, dc)
    -- hack
    coroutine.yield("hi")
    duration = duration or -1
    A = A or 32767
    dc = dc or 0

    local duration_ticks = MASTER_F // duration
    local dx = 2 * math.pi * f / MASTER_F
    local x = 0
    while x < duration_ticks or duration_ticks < 0 do
        coroutine.yield(dc + A * math.cos(x * dx))
        x = x + 1
    end
end

function mix(...)
    local end_of_stream = true
    local things = {...}
    local sum = 0
    for _, v in ipairs(things) do
        __, val = coroutine.resume(v)
        if (val ~= nil) then
            end_of_stream = false
            sum = sum + val
        end
    end

    if sum > 32767 then sum = 32767 end
    if sum < -32768 then sum = -32768 end

    return sum, end_of_stream
end

function main()
    local test1 = coroutine.create(cos_fn)
    coroutine.resume(test1, 440, -1, 10000)
    local test2 = coroutine.create(cos_fn)
    coroutine.resume(test2, 880, -1, 10000)

    local RUN = true

    while RUN do
        v, eos = mix(test1, test2)
        if not eos then
            OUTF:write(s(v))
        end
        RUN = RUN and not eos
    end
end

cs164.patch('mix', mix)
cs164.patch('cos_fn', cos_fn)
cs164.patch('naive_sq', naive_sq)
cs164.patch('main', main)

if not cs164.is_patching() then
  main()
else
  print 'patching'
end
